import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { SendMessagePage } from '../pages/send-message/send-message';
import { ReplyPage } from '../pages/reply/reply';
import { ResetPage } from '../pages/reset/reset'; 
import { AccessPage } from '../pages/access/access';
import { VerifyPage } from '../pages/verify/verify';

import { IntroPage } from '../pages/intro/intro'; 
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpModule } from '@angular/http';

import { Push } from '@ionic-native/push';

import { SMS } from '@ionic-native/sms';

import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:3001', options: {} };

import { ProfileUploadPage } from '../pages/profile-upload/profile-upload';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ProfilePage,
    SendMessagePage,
    ReplyPage,
    IntroPage,
    ResetPage,
    ProfileUploadPage,
    AccessPage,
    VerifyPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    SocketIoModule.forRoot(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ProfilePage,
    SendMessagePage,
    ReplyPage,
    IntroPage,
    ResetPage,
    ProfileUploadPage,
    AccessPage,
    VerifyPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Push,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
        File,
    Transfer,
    Camera,
    FilePath,
    SMS
  ]
})
export class AppModule {}
