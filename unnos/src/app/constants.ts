export const GlobalVariable = Object.freeze({
    WEBSERVICE_URL : 'http://vfru.in/unno_web/http/config.php',

    WEBSERVICE_DOMAIN : 'http://vfru.in/unno_web/',

    WEBSERVICE_PROFILE_PHOTO : 'http://vfru.in/unno_web/http/uploads/',
    
    LOADING_CONTENT : 'Please wait.....',

    LOGIN_STORAGE : 'unno_user_id',

    TUTORIAL_STORAGE: 'tutorial_storage',

    TOKEN_STORAGE: 'token_storage',

    HTTP_FAILED : 'Failed to connect. Try again',

    APP_NAME: 'Unnos'
});
