import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import  { GlobalVariable}  from '../app/constants';

import { AccessPage } from '../pages/access/access';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
//import { LogoutPage } from '../pages/logout/logout';
import { IntroPage } from '../pages/intro/intro';

import { Push, PushObject } from '@ionic-native/push';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = AccessPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public alertCtrl: AlertController,public push: Push) {
    this.initializeApp();
    // used for an example of ngFor and navigation
   
    
    if(localStorage.getItem(GlobalVariable.LOGIN_STORAGE)) {
    this.rootPage = HomePage;
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Profile', component: ProfilePage },
      //{ title: 'Logout', component: LogoutPage },
 
    ];
    } else {
      if(localStorage.getItem(GlobalVariable.TUTORIAL_STORAGE)) {
        this.rootPage = AccessPage;
     }  else { 
      this.rootPage = IntroPage;
     }
      this.pages = [
        { title: 'Home', component: HomePage },
        { title: 'Profile', component: ProfilePage },
       // { title: 'Logout', component: LogoutPage },
      ];
    }
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if(!localStorage.getItem(GlobalVariable.TOKEN_STORAGE)) {
        this.initPushNotification();
      }
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  initPushNotification(){
    if (!this.platform.is('cordova')) {
      console.warn("Push notifications not initialized. Cordova is not available - Run in physical device");
      return;
    }
    
    const options: any = {
      android: {
        senderID: '615820453259'
      },
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);

    pushObject.on('registration').subscribe((data: any) => {
      localStorage.setItem(GlobalVariable.TOKEN_STORAGE, data.registrationId);
    });
  
}

}
