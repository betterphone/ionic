import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';

import { GlobalVariable } from '../../app/constants';

import { SendMessagePage } from '../../pages/send-message/send-message';
import { AccessPage } from '../access/access';

import { ProfileUploadPage } from '../../pages/profile-upload/profile-upload';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  login_detail;
  data = {};
  profile_photo;
  profile_id;
  is_owner;
  photo_code;
  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController,public alertCtrl: AlertController, public loadingCtrl: LoadingController,public http: Http) {

  }

  ionViewDidLoad() {
// localStorage.clear();
    // Redirect if not login
    if(!localStorage.getItem(GlobalVariable.LOGIN_STORAGE)) {
      this.navCtrl.setRoot(AccessPage);
      return;
    }
    //
this.loginData();
  }

loginData() {
   
    this.profile_id = this.navParams.get('profile_id');
    var which_show;
        if(this.profile_id) {
          which_show = this.profile_id;
          this.is_owner = false;
        } else {
          which_show = localStorage.getItem(GlobalVariable.LOGIN_STORAGE);
          this.is_owner = true;
        }
        let loading = this.loadingCtrl.create({
          content: GlobalVariable.LOADING_CONTENT
        });
      
        loading.present();
      var http_data = 
        {
          id :which_show,
          action : 'account-detail',
          file : 'register'
        };
      
        this.http.post(GlobalVariable.WEBSERVICE_URL, {data: http_data})
        .subscribe(data => {
          loading.dismiss();
          var response = data.json();
          if(response.status == 'success') {
            this.login_detail = response.data;
            this.data = {
              name : this.login_detail.name,
              username : this.login_detail.username,
		user_name : this.login_detail.user_name,
		is_email_verified : this.login_detail.is_email_verified,
                phone : this.login_detail.phone
            };
            if(this.login_detail.is_send_message == 1) {
            this.is_send_message = true;
            } else {
              
            this.is_send_message = false;
            
            }
            if(this.login_detail.photo != null && this.login_detail.photo != '') {
              this.profile_photo = GlobalVariable.WEBSERVICE_PROFILE_PHOTO+this.login_detail.photo;
            } else {
              this.profile_photo = GlobalVariable.WEBSERVICE_PROFILE_PHOTO+'default.png';
              
            }
		this.photo_code = GlobalVariable.WEBSERVICE_DOMAIN+'photo-upload.php/?code='+this.login_detail.photo_code;
          } else {
            const alert = this.alertCtrl.create({
              title: 'Error',
              subTitle: response.message,
              buttons: ['Dismiss']
            });
            alert.present();
    
          }
          
          
        }, error => {
          loading.dismiss();
          const alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: GlobalVariable.HTTP_FAILED,
            buttons: ['Dismiss']
          });
          alert.present();
        });
    

}
 
  profileForm() {
    let loading = this.loadingCtrl.create({
      content: 'Saving.....'
    });
  
    loading.present();
    var default_data = {
      id :localStorage.getItem(GlobalVariable.LOGIN_STORAGE),
      action : 'save-detail',
      file : 'register',
    }
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: default_data, data2 : this.data})
    .subscribe(data => {
      loading.dismiss();
      var response = data.json();
      if(response.status == 'success') {
        
      } else {
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: response.message,
          buttons: ['Dismiss']
        });
        alert.present();

      }
      
      
    }, error => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: GlobalVariable.HTTP_FAILED,
        buttons: ['Dismiss']
      });
      alert.present();
    });
    
   
  }

  sendMessage(user_id) {
    var user_data = {
      user_id : user_id
    }
    this.navCtrl.push(SendMessagePage, user_data);
  }
  is_send_message;
  isSentMessage() {
    let loading = this.loadingCtrl.create({
      content: GlobalVariable.LOADING_CONTENT
    });
  
    loading.present();
    var default_data = {
      id :localStorage.getItem(GlobalVariable.LOGIN_STORAGE),
      action : 'save-isSentMessage',
      file : 'register',
      is_send_message : this.is_send_message ? '1' :'0'
    }
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: default_data})
    .subscribe(data => {
      loading.dismiss();
      var response = data.json();
      if(response.status == 'success') {
        
      } else {
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: response.message,
          buttons: ['Dismiss']
        });
        alert.present();

      }
      
      
    }, error => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: GlobalVariable.HTTP_FAILED,
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }

GoChangeProfile() {
window.open(this.photo_code);
}

verify_email() {
let loading = this.loadingCtrl.create({
      content: GlobalVariable.LOADING_CONTENT
    });
  
    loading.present();
    var default_data = {
      id :localStorage.getItem(GlobalVariable.LOGIN_STORAGE),
      action : 'verfiy-email',
      file : 'register',
    }
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: default_data})
    .subscribe(data => {
      loading.dismiss();
      var response = data.json();
      if(response.status == 'success') {
        const alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: response.message,
          buttons: ['Dismiss']
        });
        alert.present();
      } else {
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: response.message,
          buttons: ['Dismiss']
        });
        alert.present();

      }
      
      
    }, error => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: GlobalVariable.HTTP_FAILED,
        buttons: ['Dismiss']
      });
      alert.present();
    });
}
  photoUpload() {
    this.navCtrl.push(ProfileUploadPage);
  }

}
