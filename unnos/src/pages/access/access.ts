import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SMS } from '@ionic-native/sms';

import { VerifyPage } from '../verify/verify'; 

@IonicPage()
@Component({
  selector: 'page-access',
  templateUrl: 'access.html',
})
export class AccessPage {
   
  otpCreated = false; 
  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private sms: SMS
     ) {
  }

  ionViewDidLoad() {
   
  }

data = {
phone : ''
};
loginForm() {

   if(this.data.phone) {
    
      var otp = Math.floor(Math.random() * 9999);

      // Send a text message using default options
      this.sms.send(this.data.phone, otp + ' is your login otp.');
	this.navCtrl.push(VerifyPage, {phone : this.data.phone, otp : otp});
   }
}

}
