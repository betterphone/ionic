import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { HomePage } from '../home/home'; 

import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';

import { GlobalVariable } from '../../app/constants';

@IonicPage()
@Component({
  selector: 'page-reset',
  templateUrl: 'reset.html',
})
export class ResetPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController,public alertCtrl: AlertController, public loadingCtrl: LoadingController,public http: Http) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPage');
  }

data = {
    'action' : 'reset',
    'file' : 'register',
    'device_token' : localStorage.getItem(GlobalVariable.TOKEN_STORAGE)
  }
  resetForm() { 

    if(this.data) {
      let loading = this.loadingCtrl.create({
        content: GlobalVariable.LOADING_CONTENT
      });
    
      loading.present();
    
    var http_data = this.data;
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: http_data})
    .subscribe(data => {
      loading.dismiss();
      var response = data.json();
      if(response.status == 'success') {
        localStorage.setItem(GlobalVariable.LOGIN_STORAGE, response.user_id);
        this.navCtrl.setRoot(HomePage);
      } else {
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: response.message,
          buttons: ['Dismiss']
        });
        alert.present();

      }
      
      
    }, error => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Failed to connect. Try again',
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }

 
}


}

