import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileUploadPage } from './profile-upload';

@NgModule({
  declarations: [
    ProfileUploadPage,
  ],
  imports: [
    IonicPageModule.forChild(ProfileUploadPage),
  ],
})
export class ProfileUploadPageModule {}
