import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AccessPage } from '../access/access';
import { GlobalVariable } from '../../app/constants';


@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  slides = [
    {
      title: "Welcome to the "+GlobalVariable.APP_NAME+"!",
      description: "",
image : 'assets/images/logo.png'
    },
    {
      title: "What is "+GlobalVariable.APP_NAME+"?",
      description: "Communicate anonymously with your colleagues, friends and anyone you want.",
image : 'assets/images/what-is.png'
    },
    
  ];

  finishTutorial(){
    localStorage.setItem(GlobalVariable.TUTORIAL_STORAGE, '1');
    this.navCtrl.setRoot(AccessPage);
  }

}
