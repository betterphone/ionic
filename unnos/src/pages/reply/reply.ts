import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular'
import { Http } from '@angular/http';
import { GlobalVariable } from '../../app/constants';

@IonicPage()
@Component({
  selector: 'page-reply',
  templateUrl: 'reply.html',
})
export class ReplyPage {
  from_user_id;
  messages;
  receiver_detail;
  sender_detail;
  WEBSERVICE_PROFILE_PHOTO;
  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController,public alertCtrl: AlertController, public loadingCtrl: LoadingController,public http: Http) {
    
   
  }

ionViewDidLoad() {


 this.from_user_id = this.navParams.get('user_id');

    this.WEBSERVICE_PROFILE_PHOTO = GlobalVariable.WEBSERVICE_PROFILE_PHOTO;

    let loading = this.loadingCtrl.create({
      content: GlobalVariable.LOADING_CONTENT
    });
    
    var messages_data = {
      action : 'received-messages-single-user',
      file : 'message',
      to_user_id :localStorage.getItem(GlobalVariable.LOGIN_STORAGE),
      from_user_id : this.from_user_id
    };
    loading.present();
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: messages_data})
    .subscribe(data => {
      loading.dismiss();
      var response = data.json();
      if(response.status == 'success') {
        this.messages = response.data.reverse();
        this.receiver_detail = response.receiver_detail;
        this.sender_detail = response.sender_detail;
       
this.scrollToBottom();
      }
      
    }, error => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: GlobalVariable.HTTP_FAILED,
        buttons: ['Dismiss']
      });
      alert.present();
    });
    
}

  delete_send_message (message_id) {

let alert = this.alertCtrl.create({
    title: 'Confirm Delete',
    message: 'Do you want to delete this message?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
        }
      },
      {
        text: 'Delete',
        handler: () => {
          if(message_id) {
      let loading = this.loadingCtrl.create({
        content: GlobalVariable.LOADING_CONTENT
      });
    
      loading.present();
    
    var http_data = {
      action : 'delete-message',
      file : 'message',
      message_id : message_id,
      login_user_id :localStorage.getItem(GlobalVariable.LOGIN_STORAGE),
    };
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: http_data})
    .subscribe(data => {
      loading.dismiss();
      var response = data.json();
      if(response.status == 'success') {
	this.ionViewDidLoad();
      }
      
      
    }, error => {
      loading.dismiss();
      
    });
  }
        }
      }
    ]
  });
  alert.present();
    
  }
  data = {
    action : 'send-message-reply',
    file : 'message',
    to_user_id :localStorage.getItem(GlobalVariable.LOGIN_STORAGE),
    is_replied : localStorage.getItem(GlobalVariable.LOGIN_STORAGE),
    message : ''
   }
  messageForm(from_user_id) {
    let loading = this.loadingCtrl.create({
      content: GlobalVariable.LOADING_CONTENT
    });
    
    loading.present();
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: this.data, data2 : from_user_id})
    .subscribe(data => {
      loading.dismiss();
      var response = data.json();
      if(response.status == 'success') {
        
        this.data.message = '';
	this.ionViewDidLoad();
      } else {
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: response.message,
          buttons: ['Dismiss']
        });
        alert.present();

      }
      
      
    }, error => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: GlobalVariable.HTTP_FAILED,
        buttons: ['Dismiss']
      });
      alert.present();
    });
  }

  doRefresh(refresher) {
   this.ionViewDidLoad();

   setTimeout(() => {
      refresher.complete();
    }, 1000);
  }

scrollToBottom() {
  var element = document.getElementById("content-bottom-ConversationPageReply");
 
  setTimeout(()=>{element.scrollIntoView(true)},200); 
}
  
}
