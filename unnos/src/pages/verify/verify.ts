import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AlertController, ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular'
import { Http } from '@angular/http';
import { GlobalVariable } from '../../app/constants';

import { ProfilePage } from '../profile/profile';

@IonicPage()
@Component({
  selector: 'page-verify',
  templateUrl: 'verify.html',
})
export class VerifyPage {
send_otp;
data = {
phone : '',
otp : '',
    action : 'success-otp',
    file : 'register',
    device_token : localStorage.getItem(GlobalVariable.TOKEN_STORAGE)
};
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
		public alertCtrl: AlertController,
               public loadingCtrl: LoadingController,
              public http: Http,
              public toastCtrl: ToastController
) {
  }

  ionViewDidLoad() {
    this.data.phone = this.navParams.get('phone');
    this.send_otp = this.navParams.get('otp');

//alert(this.send_otp);
  }


otpForm() {
	if(this.data.otp) {
		if(this.data.otp == this.send_otp) {
// Send Request

      let loading = this.loadingCtrl.create({
        content: GlobalVariable.LOADING_CONTENT
      });
    
      loading.present();
    
    var http_data = this.data;
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: http_data})
    .subscribe(data => {
      loading.dismiss();
      var response = data.json();
      if(response.status == 'success') {
        localStorage.setItem(GlobalVariable.LOGIN_STORAGE, response.user_id);
        this.navCtrl.setRoot(ProfilePage);
      
this.presentToast(response.message);
      } else {
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: response.message,
          buttons: ['Dismiss']
        });
        alert.present();

      }
      
      
    }, error => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: GlobalVariable.HTTP_FAILED,
        buttons: ['Dismiss']
      });
      alert.present();
    });

//---------------
		}	else {
	const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'OTP is not correct.',
          buttons: ['Dismiss']
        });
        alert.present();
}
	}
}

presentToast(message) {
  let toast = this.toastCtrl.create({
    message: message,
    duration: 3000,
    position: 'bottom'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
}

}
