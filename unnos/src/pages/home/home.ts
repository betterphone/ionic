import { Component } from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';

import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular'
import { Http } from '@angular/http';
import { GlobalVariable } from '../../app/constants';

import { SendMessagePage } from '../send-message/send-message';
import { ReplyPage } from '../reply/reply';
import { ProfilePage } from '../profile/profile';
import { AccessPage } from '../access/access';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  sent_messages;
  send_messages;
  sender_detail;
  APP_NAME;
searchLoading = false;
  WEBSERVICE_PROFILE_PHOTO;
  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController,public alertCtrl: AlertController, public loadingCtrl: LoadingController,public http: Http) {
  }

ionViewDidLoad() {
    
        // Redirect if not login
        if(!localStorage.getItem(GlobalVariable.LOGIN_STORAGE)) {
          this.navCtrl.setRoot(AccessPage);
          return;
        }
        //
this.APP_NAME = GlobalVariable.APP_NAME;
    this.WEBSERVICE_PROFILE_PHOTO = GlobalVariable.WEBSERVICE_PROFILE_PHOTO;

    let loading = this.loadingCtrl.create({
      content: GlobalVariable.LOADING_CONTENT
    });
    
    loading.present();
    var http_data = {
      action : 'sent-messages',
      file : 'message',
      login_user_id :localStorage.getItem(GlobalVariable.LOGIN_STORAGE),
    };
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: http_data})
    .subscribe(data => {
      loading.dismiss();
      var response = data.json();
      if(response.status == 'success') {
        this.send_messages = response.send_messages;
        this.sent_messages = response.sent_messages;
        this.sender_detail = response.sender_detail;
        
        
      } else {
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: response.message,
          buttons: ['Dismiss']
        });
        alert.present();

      }
      
      
    }, error => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: GlobalVariable.HTTP_FAILED,
        buttons: ['Dismiss']
      });
      alert.present();
    });

}

  messages: string = "received";
  search_users;
  search(ev: any) {
    let keyword = ev.target.value;
  
    if(keyword) {
      this.searchLoading = true;
    
    var http_data = {
      action : 'search-user',
      file : 'search',
      keyword : keyword,
      login_user_id :localStorage.getItem(GlobalVariable.LOGIN_STORAGE),
    };
    this.http.post(GlobalVariable.WEBSERVICE_URL, {data: http_data})
    .subscribe(data => {
      this.searchLoading = false;
      var response = data.json();
      if(response.status == 'success') {
        this.search_users = response.data;
      } else {
        this.search_users = [];
      }
      
      
    }, error => {
      this.searchLoading = false;
      
    });
  }
}
sendMessage(user_id) {
  var user_data = {
    user_id : user_id
  }
  this.navCtrl.push(SendMessagePage, user_data);
}
replyPage(from_user_id) {
  var from_user_data = {
    user_id : from_user_id
  }
  this.navCtrl.push(ReplyPage, from_user_data);
}

goProfile(profile_id) {
  var profile_data = {
    profile_id : profile_id
  };
  this.navCtrl.push(ProfilePage, profile_data);
}


  doRefresh(refresher) {
   this.ionViewDidLoad();

   setTimeout(() => {
      refresher.complete();
    }, 1000);
  }


}
